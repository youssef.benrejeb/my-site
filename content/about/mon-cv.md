---
title: "Mon Cv"
subtitle: ""
date: 2022-11-02T10:55:56+01:00
lastmod: 2022-11-02T10:55:56+01:00
draft: true
author: "Youssef BEN"
authorLink: ""
description: ""
license: ""
images: []

tags: [CV]
categories: [About Me]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true  
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

# Youssef BEN REJEB
Responsable Technique / Testeur fonctionnel   
#26, Avenue de Paris   
#92320 Châtillon   
#(33) 6 67 20 59 93   
#youssef.benrejeb@gmail.com   

### Profil

Plus de 18 années d’expérience dans le domaine de l'informatique, testeur certifié niveau fondation ISTQB.
J'ai travaillé deux ans comme responsable du processus d'automatisation, désormais, je cherche un poste de testeur fonctionnel.

### EXPÉRIENCE

**INOVERNA, France** — *Consultant QA et automatisation*  
<font size ="2">OCTOBRE  2022 À AUJOURD'HUI</font>

**TOPNET, Tunis** — *Testeur QA et responsable du processus d'automatisation*  
<font size ="2">JUILLET 2021 À OCTOBRE 2022</font>
- Automatiser les  tâches répétitives ainsi que le flux de travail au sein de l’entreprise ce qui a évalué l'efficacité de traitement des réclamations client  niveau HelpDesk à plus de 70 %.
- Préparer et effectuer les tests nécessaires en mode manuel ou automatique, de manière formelle ou informelle afin d'identifier les dysfonctionnements et défauts.

**TOPNET, Tunis** — *Chef Service support  N3 ( Xdsl )*  
<font size ="2">JUILLET 2016 À JUIN 2021</font>
- Coordonner une équipe de 30 techniciens.
- Définir et optimiser les procédures d’assistance.
- Planifier des interventions sur site si nécessaire.
- Assurer un accompagnement de proximité pour les clients PRO.

**TOPNET, Tunis** — *Technicien systèmes et réseaux*   
<font size ="2">JUILLET 2009 À JUIN 2016</font>
- Établir un diagnostic et apporter des solutions adaptées à l’incident observé.
- Assister les clients lors d’une panne éventuelle.
- Résoudre les problèmes qui n’ont pas pu être résolus au niveau 2 en rapport avec la plateforme mail, DNS, hébergement dédié et mutualisé. 

**TOPNET, Tunis** — *Webmaster*  
<font size ="2">JUILLET 2007 À JUIN 2009</font>
- Assurer la maintenance, la sécurité et la mise à jour du site internet et intranet de TOPNET.
- Maintenir la plateforme technique.
- S'occuper du référencement, en collaboration avec les web designers et les développeurs...

**TOPNET, Tunis** — *Technicien Support N2 ( hébergement web et mail )*  
<font size ="2">JUILLET 2005 À JUIN 2007</font>
- Résoudre les problèmes qui n’ont pas pu être résolus au N1. 
- Identifier et corriger les sources d’erreurs ou de panne.
- Émission des demandes d’actions préventives de fond.

**TOPNET, Tunis** — *Technicien Support N1*  
<font size ="2">JUILLET 2004 À JUIN 2005</font>
- Enregistrement des incidents ou anomalies de fonctionnement signalés. 
- Résolution de pannes ou d’incidents, et accompagnement des clients
- Pré-diagnostic et transfert des appels des clients aux services concernés.

### FORMATION

[Institut Supérieur des Etudes Technologiques, Sfax](https://isetsf.rnu.tn/) — <font size ="2">DÉCEMBRE 2021</font>      

{{< admonition note "Master professionnel en  Réseaux Informatiques & Nouvelles Technologies" >}}
Programme de deux ans spécialité : Réseaux Informatiques.  
Il présente une continuité des études proposées au niveau licence dans le domaine des technologies de l’informatique et des réseaux.
{{< /admonition >}}

[Institut Supérieur des Etudes Technologiques, Mahdia](https://isetma.rnu.tn/) — <font size ="2">DÉCEMBRE 2016</font>  

{{< admonition note "License Appliquée en Technologies de l’Informatique (TI)">}}
Programme d’un an spécialité : Développement des Systèmes d’Information (DSI) : orienté vers le domaine de l’Informatique de Gestion.
Il présente une continuité des études proposées au niveau technicien supérieur.
{{< /admonition >}}

[Institut Supérieur des Etudes Technologiques , Sousse](https://isetso.rnu.tn/) — <font size ="2">MAI 2004</font>

{{< admonition note "Technicien Supérieur En Informatique">}}
Programme de deux ans et demi spécialité : Informatique Industrielle.
{{< /admonition >}}

### CERTIFICAT

[**ISTQB**](https://www.gasq.org/en/registration/expert/0c73ab6c-49ed-4d3e-88ef-9dacce608615.html)  : version 2018 du Syllabus de Niveau Fondation de Testeur Certifié  
<font size ="2">JUIN 2022</font>

### COMPÉTENCES
Tests manuels / automatisés, Méthodes agiles, Jira,  Python, Squash ™, Testlink, Katalon, Selenium, Uipath, Power automate, CI / CD ( Multipass, Docker, Jenkins, Gitlab, Github ...), Hugo, Render. Netlify.

### LANGUES
Français.  
Anglais B1.

### CENTRE D’INTÉRÊT
Les gadgets

<!--more-->
