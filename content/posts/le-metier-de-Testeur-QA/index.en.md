---
title: "Le metier de Testeur QA"
subtitle: ""
date: 2022-11-01T11:15:36+01:00
lastmod: 2022-11-01T11:15:36+01:00
draft: false
author: "Youssef BEN"
authorLink: ""
description: ""
license: ""
images: []

tags: ["Test","QA"]
categories: [QA]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---

<!--more-->

## Qu'est ce qu'un Testeur QA, quelles sont ses missions ?
Dans le monde de la conception de logiciels, de jeux vidéo, de plateformes, de sites ou encore d'autres produits et services, le testeur QA, pour Quality Assurance, a pour rôle de tester l’ensemble des fonctionnalités de l’outil et de mettre en place un suivi des itérations/modifications.

Il doit ainsi déterminer les bugs et tous les problèmes et anomalies qui se présentent pendant une utilisation normale, afin de les réparer avant que ne sorte le logiciel. Pour cela :

- Il étudie l’ensemble des fonctionnalités en les testant une à une et en multipliant les possibilités. De cette façon, il se met à la place de tous les utilisateurs, comme s’il tentait de trouver un code parmi une multitude de combinaisons.
- Il effectue un reporting de chacune des problématiques afin que les ingénieurs trouvent des solutions.
- Il dispose également d'un rôle de conseil pour l’amélioration du produit avant sa sortie sur le marché.
- Puis, il doit à nouveau réaliser des tests pour valider les modifications.
- Il peut aussi être amené à déterminer le planning des modifications en fonction de ses tests.

D’une certaine manière, le testeur QA fait le lien entre l’utilisateur - dont il doit comprendre et reproduire les comportements - et les développeurs.

## Quels sont les principaux interlocuteurs du Testeur QA ?
Le testeur QA travaille au sein de l’équipe de développement. Il est amené à collaborer en permanence avec les ingénieurs QA, les développeurs web ou développeurs logiciel / Software engineers, et doit rendre des comptes à son manager qui peut être le Responsable QA, un Lead developer, le CTO, le CIO, ou dans certains cas un Chef de Projet Technique.

Dans une startup, comme dans une grande agence, le testeur QA fait partie des fonctions indispensables au bon développement des produits informatiques.
Au sein de petites structures ou d'entreprises naissantes, le rôle de testeur est parfois assuré par le(s) développeur(s).

## Quelles sont les compétences et qualités requises pour être Testeur QA ?
La principale qualité d’un testeur QA est sa rigueur face à l’abondance de possibilités que nécessitent ses tests et aux comptes rendus qu’il doit transmettre aux équipes de conception.

Il doit aussi avoir un minimum de compétences techniques dans le développement de logiciels pour être en mesure de détecter toutes les anomalies. À la fois travailleur solitaire concentré et membre d’une équipe, il doit passer de l’un à l’autre constamment.
Selon les entreprises, les produits et les missions qui lui sont confiées, le Testeur QA peut avoir plus ou moins de compétences techniques : du "simple" test manuel jusqu'aux tests automatisés, il y a parfois de grandes différences dans les connaissances techniques et les outils à maîtriser.

Le testeur QA doit dans tous les cas avoir des compétences créatives et développer une fine analyse des comportements des utilisateurs pour les imiter au mieux dans ses tests. Cela demande donc une approche à la fois technique et psychologique.

## Quelles études ou formations pour devenir Testeur QA ?
Devenir testeur QA nécessite d’avoir une formation minimum en informatique comme par exemple :

- BTS informatique
- Licence professionnelle systèmes informatiques et logiciel
- Master 2 professionnel informatique
- École d’ingénieur en programmation informatique

Le testeur QA a aussi tout intérêt à se former régulièrement et à obtenir des certifications de type CFTL/ISTQB.

Contrairement à ce que l’on pourrait croire, notamment pour les testeurs QA de jeux vidéos, les profils recherchés sont très spécifiques. Car il ne s’agit pas seulement de tester l’outil, mais d’en connaître toutes les caractéristiques techniques.

## Quelles sont les évolutions professionnelles et perspectives de carrière d'un Testeur QA ?
Le métier de testeur QA peut mener à une carrière "fonctionnelle" / managériale en tant que :

- Chef de projet
- Test Lead
- Responsable d'une équipe QA
- Responsable Test et Validation
- Et à des postes plus techniques qui nécessitent généralement une plus grande expertise informatique :

- Ingénieur QA
- Ingénieur Test et Validation
- De Testeur Manuel à la maîtrise des Tests automatisés
- Responsable Technnique QA
- Testing Expert
- Directeur Technique

## Quels sont les principaux secteurs d'activités et employeurs d'un Testeur QA ?
Un testeur QA évolue au sein :

- D’éditeurs de jeux vidéo.
- D’éditeurs de logiciels.
- D’opérateurs téléphoniques.
- D’agences Web.
- D’agences ESN/SSII
- D’autres entreprise proposant des services informatiques.

A noter qu'il est un peu plus rarement intégré directement dans un grand groupe car les grandes sociétés externalisent souvent cette fonction. Mais les prochaines années devraient voir apparaître de plus en plus des testeurs QA salariés de grandes entreprises qui investissent (enfin) dans le numérique.

## Source :: [anakine.io](https://anakine.io/fiches-metiers-tech/tout-savoir-sur-le-metier-de-testeur-qa/)