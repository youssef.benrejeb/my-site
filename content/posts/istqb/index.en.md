---
title: "ISTQB"
subtitle: ""
date: 2022-11-01T15:40:21+01:00
lastmod: 2022-11-01T15:40:21+01:00
draft: false
author: "Youssef BEN"
authorLink: ""
description: ""
license: ""
images: []

tags: [QA,ISTQB]
categories: ["QA","documentation"]


featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: false
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---
L'ISTQB (en anglais « International Software Testing Qualifications Board ») est le Comité international de qualification du test logiciel. Cette organisation propose une certification reconnue dans le monde entier.

## Formations à l’ISTQB gratuites
Tout d’abord je tiens à remercier toutes les personnes qui ont contribué à cette formation et surtout  monsieur [Youssef Touati](https://www.linkedin.com/in/youssef-touati/) pour l’effort et le temps qu’il a consacré pour nous proposer ce contenu gratuit et efficace. 

[Apprendre les tests de logiciels avec Youssef](https://www.youtube.com/channel/UCof5f75LV1NzQsAsKUMndRw/featured)


###  Initiation
{{< youtube hq1tzZKI3MU >}}

###  Chapitre 1 - *Les fondamentaux du test*
{{< youtube dFIpNZ-7S2Y >}}

###  Chapitre 2 - *Tester pendant le cycle de vie du développement logiciel*
{{< youtube 8z-LLmLB73o >}}

###  Chapitre 3 -*Les Tests Statiques*
{{< youtube nKt6Q6dhz0Q >}}

### Chapitre 4 - *Les Techniques de test* 
  - *Partie 1*
{{< youtube hTlqHZ4oXZY >}}  

  - *Partie 2*
{{< youtube g8dMSaukFiQ >}}  

###  Chapitre 5 - *La gestion des tests*
{{< youtube EA5raR_b98Y >}}

###  Chapitre 6 - *Outils de support aux tests*
{{< youtube pxlH_czp1ws >}}

## Autres Formations à l’ISTQB
- [Udemy](https://www.qualitelogiciel.com/qualitelogiciel-istqb)
<!--more-->
