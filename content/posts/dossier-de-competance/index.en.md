---
title: "Dossier de compétances"
subtitle: ""
date: 2022-11-03T09:09:33+01:00
lastmod: 2022-11-03T09:09:33+01:00
draft: false
author: "Youssef BEN"
authorLink: ""
description: ""
license: ""
images: []
resources:
- name: "featured-image"
  src: "featured-image.jpg"

tags: [CV]
categories: [About Me]

featuredImage: ""
featuredImagePreview: ""

hiddenFromHomePage: true
hiddenFromSearch: false
twemoji: false
lightgallery: true
ruby: true
fraction: true
fontawesome: true
linkToMarkdown: true
rssFullText: false

toc:
  enable: true
  auto: true
code:
  copy: true
  maxShownLines: 50
math:
  enable: false
  # ...
mapbox:
  # ...
share:
  enable: true
  # ...
comment:
  enable: true
  # ...
library:
  css:
    # someCSS = "some.css"
    # located in "assets/"
    # Or
    # someCSS = "https://cdn.example.com/some.css"
  js:
    # someJS = "some.js"
    # located in "assets/"
    # Or
    # someJS = "https://cdn.example.com/some.js"
seo:
  images: []
  # ...
---
<div style="text-align:right;"><i>Youssef BEN REJEB</i><br>
<i>Responsable Technique / Testeur QA</i><br> 
<i>youssef.benrejeb@gmail.com</i><br></div>

# EXPERIENCE PROFESSIONNELLE 


## De 07/2021 à 10/2022 
*Testeur QA et responsable du processus d'automatisation.*

### Projet
Confier un maximum de tâches répétitives du service d’assistance (Helpdesk) et à faible valeur ajoutée à des systèmes informatiques plutôt qu'à des collaborateurs.

### Descriptif
- Identifier la liste des processus métier qui nécessitent une automatisation.
- Automatisation des tâches répétitives relativement simple via le RPA.
- Développement des scripts d’automation des flux de travail.
- Préparer les activités de tests prévues ( plan de test ).
- Effectuer les tests automatisés et manuelles ( unitaire, non régression, intégration, système et d’acceptation ).
- Rendre compte des résultats des tests par le biais de rapports d'exécution des tests.
- Assurer l’intégration et le déploiement continu.
- Mettre en place des outils de support par ticket pour les partenaires techniques ( Osticket, Webcalendar …. ).

### Environnement Technique
Python, Api, Postman, Json, Selenium, Xpath, Katalon, Jenkins, Multipass, Docker, Portainer, Github, Gitlab, RPA (Power Automate, Uipath ), Jira, méthodes Agile, CI CD, Testlink, Squash ™, Hugo et Render.


## De 07/2016 à 06/2021 
*Chef Service support technique N3 ( Xdsl )*  
 
### Projet 
Assure l’assistance et le support technique auprès de la clientèle.

### Descriptif
- Résoudre les problèmes qui n’ont pas pu être résolus au N2
- Intervient en cas de panne et/ou coordonne les équipes d’intervention mobile
- Organiser et programmer les activités et opérations de maintenance.
- Aider et conseiller les clients lors de la mise en fonctionnement de leur connexion internet.
- Coordonner entre les sous traitants et les équipes techniques.
### Environnement Technique
Modem ( D-link, Tp-link et Cisco ), Microsoft CRM et les Workflows en général.


## De 07/2009 à 06/2016
*Technicien systèmes et réseaux*

### Projet
Déploiement, surveillance, migration, test et support N2 et N3.

### Descriptif
- Accompagner les clients  dans la prise en main de leurs serveurs.
- Contribuer au test exploratoire et d’acceptation. 
- Assister les clients dans la migration de leurs sites vers les nouvelles plateformes. 
- Résoudre les problèmes qui n’ont pas pu être résolus au N2.
- Assurer des configurations spécifiques selon le besoin des client
- Contribuer à la formation et à la montée en compétences des clients ainsi que le support N2.
- Diagnostiquer et résoudre les pannes, problèmes de sécurité et dysfonctionnements relevant des systèmes ou du réseau côté plateforme.
- Assurer une veille technique et sécuritaire continue (sauvegarde, analyse des logs et journaux, étude des alertes, etc.).
- Développement d’une interface Web de Gestion automatisée de la configuration via Webmin.
- Assurer le support N2 des VPS ainsi que les serveurs Mails, DNS et  Web  (dédiés ou mutualisés) 

### Environnement Technique
Php, Phpmyadmin, MySql, Mrtg, Nagios, Zabbix, SNMP, POP3, IMAP, SMTP, API, Qmail, Postfix, Roundcube, Barracuda Spam Firewall, Webmin, Bind, DNS, Pure-FTPd, Apache, LAMP, Syslog, Xen, VmWare, Parallels Virtuozzo, Parallels Business Automation, Plesk Panel, VPS Hosting, Kloxo, Fail2ban, logiciels Open source en général.


## De 07/2007 à 06/2009
*Webmaster*

### Projet
la maintenance du site internet de TOPNET et de sa mise à jour, ainsi que l’intégration de contenus éditoriaux et illustratifs.

### Descriptif 
- Configurer et maintenir le serveur d’hébergement.
- Chargement et mise à jour des contenus via CMS.
- Surveiller quotidiennement les activités du site web à l'aide d'instruments analytiques.
- Optimiser les performances, 
- Gestion des newsletter et du mailing list.
- Web development. 
- Web design.
- Entretien et assistance technique en cas de bug ou d'erreurs du serveur. 
- Suivi de l'accessibilité et de la sécurité du site web

### Environnement Technique
Php, Phpmyadmin, MySql, API, Joomla, Wordpress, Drupal, PrestaShop, Dreamweaver, Photoshop, illustrator, Mrtg, Nagios, Zabbix. 


## De 07/2005 à 06/2007 
*Technicien Support N2 ( hébergement web et mail )*

### Projet
Assurer le support et l'assistance technique du mail ainsi que l’hébergement web.

### Descriptif
- Résoudre les problèmes qui n’ont pas pu être résolus au N 1.
- Renseigner les clients par email et/ou par téléphone.
- Diagnostiquer les pannes.
- Cerner, détecter les incidents et apporter rapidement une solution technique et/ou commerciale
- Remonter à l’Administrateur Systèmes & Réseaux les problèmes rencontrés nécessitant leur intervention en respectant la chaîne d’escalade
- Identifier les besoins en formation et les acheminer à l’équipe de formation.

### Environnement Technique
Windows, linux, OTRS, CRM, Webmin, Radmin, cpanel plesk.  


## De 07/2004 à 06/2005 
*Technicien Support N1*

### Projet
Réaliser le premier contact avec les clients par téléphone, mail ou portail en ligne

### Descriptif
- Accueil des demandes des clients suite à des anomalies ou dysfonctionnements.
- Résolution de problèmes non bloquants.
- Enregistrement des incidents signalés.
- Pré-qualification des dysfonctionnements pour orientation vers la cellule concernée.
- Traitement ou déclenchement des actions de support correspondantes.
- Suivi des incidents : suivi du traitement des appels des clients.
- Information des clients : alerte, information du client. Diffusion d’informations.
- Autres tâches d’infogérance.

### Environnement Technique
Windows, linux, OTRS. 

<!--more-->
